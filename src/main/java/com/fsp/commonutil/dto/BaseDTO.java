/*
 * Copyright Farmacias San Pablo
 * 30-09-2019
 */
package com.fsp.commonutil.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Abstract class is responsible for containing common fields for DTO type classes
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see Serializable
 * @see Date
 * @see Boolean
 */
public abstract class BaseDTO implements Serializable {

	private static final long serialVersionUID = -2216318005730102659L;
	protected Integer id;
	protected Date creationDate;
	protected Date modificationDate;
	protected Boolean status;
	
	/**
	 * Method responsible for returning the id attribute
	 * @return Integer
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Method responsible for assigning a value to the id attribute
	 * @param Integer id
	 */
	public void setId(final Integer id) {
		this.id = id;
	}
	
	/**
	 * Method responsible for returning the creationDate attribute
	 * @return Date
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Method responsible for assigning a value to the creationDate attribute
	 * @param Date creationDate
	 */
	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}
	
	/**
	 * Method responsible for returning the id modificationDate
	 * @return Date
	 */
	public Date getModificationDate() {
		return modificationDate;
	}
	
	/**
	 * Method responsible for assigning a value to the modificationDate attribute
	 * @param Date modificationDate
	 */
	public void setModificationDate(final Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	
	/**
	 * Method responsible for returning the status attribute
	 * @return Boolean
	 */
	public Boolean getStatus() {
		return status;
	}
	
	/**
	 * Method responsible for assigning a value to the status attribute
	 * @param Boolean status
	 */
	public void setStatus(final Boolean status) {
		this.status = status;
	}

}