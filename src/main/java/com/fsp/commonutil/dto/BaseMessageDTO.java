/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.commonutil.dto;

import java.io.Serializable;

/**
 * Abstract class in charge of being the super type for message replies
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see Serializable
 * @see Boolean
 */
public abstract class BaseMessageDTO implements Serializable {

	private static final long serialVersionUID = -1118761438015194451L;
	protected Boolean result;
	
	/**
	 * Method responsible for returning the assigned value as a result
	 * @return Boolean, stored result
	 */
	public Boolean getResult() {
		return result;
	}

	/**
	 * Method responsible for assigning a value to the result variable
	 * @param result, value to assign
	 */
	public void setResult(final Boolean result) {
		this.result = result;
	}

}