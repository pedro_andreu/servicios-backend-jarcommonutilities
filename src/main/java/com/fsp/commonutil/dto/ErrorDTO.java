/*
 * Copyright Farmacias San Pablo
 * 30-09-2019
 */
package com.fsp.commonutil.dto;

import java.io.Serializable;

/**
 * Transport type class type class responsible for encapsulate the properties of an error
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see Serializable
 * @see String
 * @see Integer
 */
public class ErrorDTO implements Serializable {

	private static final long serialVersionUID = 1168980165404670075L;
	
	private String type;
	private String status;
	private Integer code;
	private String url;
	private String message;
	
	/**
	 * Constructor with parameters
	 * @param type, type of error
	 * @param status, error status
	 * @param code, error http code
	 * @param url, url where the error was generated
	 * @param message, error message
	 * @see String
	 * @see Integer
	 */
	public ErrorDTO(
			final String type, 
			final String status, 
			final Integer code, 
			final String url, 
			final String message) {
		this.setType(type);
		this.setStatus(status);
		this.setCode(code);
		this.setUrl(url);
		this.setMessage(message);
	}

	/**
	 * Method responsible for returning the assigned value as a type
	 * @return String, type of error
	 */
	public String getType() {
		return type;
	}

	/**
	 * Method responsible for assigning a value to the type variable
	 * @param type, type of error
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Method responsible for returning the assigned value as a status
	 * @return String, error status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Method responsible for assigning a value to the status variable
	 * @param status, error status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Method responsible for returning the assigned value as a code
	 * @return Integer, error http code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Method responsible for assigning a value to the code variable
	 * @param code, error http code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Method responsible for returning the assigned value as a url
	 * @return String, url where the error was generated
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Method responsible for assigning a value to the url variable
	 * @param url, url where the error was generated
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Method responsible for returning the assigned value as a message
	 * @return String, error message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Method responsible for assigning a value to the message variable
	 * @param message, error message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}