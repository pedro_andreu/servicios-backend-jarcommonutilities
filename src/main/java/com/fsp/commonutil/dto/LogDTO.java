/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.commonutil.dto;

import java.io.Serializable;

/**
 * Transport type class responsible for encapsulating the properties of the log error
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see Serializable
 * @see String
 */
public class LogDTO implements Serializable {
	
	private static final long serialVersionUID = -6227853459772181709L;
	private String date;
	private String httpCode;
    private Integer errorCode;
	private String message;
	private String trace;
	private String uuid;
	
	/**
	 * Method responsible for assigning a value to the date attribute
	 * @param String date
	 */
	public LogDTO date(final String date) {
		this.date = date;
		return this;
	}
	
	/**
	 * Method responsible for assigning a value to the httpCode attribute
	 * @param String httpCode
	 */
	public LogDTO httpCode(final String httpCode) {
		this.httpCode = httpCode;
		return this;
	}
	
	/**
	 * Method responsible for assigning a value to the errorCode attribute
	 * @param Integer errorCode
	 */
	public LogDTO errorCode(final Integer errorCode) {
		this.errorCode = errorCode;
		return this;
	}
	
	/**
	 * Method responsible for assigning a value to the message attribute
	 * @param String message
	 */
	public LogDTO message(final String message) {
		this.message = message;
		return this;
	}
	
	/**
	 * Method responsible for assigning a value to the trace attribute
	 * @param String trace
	 */
	public LogDTO trace(final String trace) {
		this.trace = trace;
		return this;
	}
	
	/**
	 * Method responsible for assigning a value to the UUID attribute
	 * @param String UUID
	 */
	public LogDTO uuid(final String uuid) {
		this.uuid = uuid;
		return this;
	}
	
	/**
	 * Method responsible for returning as string all object attributes
	 * @return String
	 */
	@Override
	public String toString() {
		return "GlobalLogException [uuid = " + uuid + ", date = " + date + ", httpCode = " + httpCode + ", errorCode = " + errorCode + ", message = "
				+ message + ", trace = " + trace + "]";
	}

}