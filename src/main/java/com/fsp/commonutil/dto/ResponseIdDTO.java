/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.commonutil.dto;

/**
 * Transport type class type class responsible for returning a message of type Integer 
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see BaseMessageDTO
 * @see Integer
 */
public class ResponseIdDTO extends BaseMessageDTO {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	/**
	 * Constructor with parameters
	 * @param result, result of an operation
	 * @param id, identifier to return
	 * @see Boolean
	 * @see Integer
	 */
	public ResponseIdDTO(final Boolean result, final Integer id) {
		this.setResult(result);
		this.setId(id);
	}

	/**
	 * Method responsible for returning the value of the id attribute
	 * @return id, identifier to return
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Method responsible to assign the value of the id attribute
	 * @param id, identifier to return
	 */
	public void setId(Integer id) {
		this.id = id;
	}

}