/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.commonutil.dto;

import java.io.Serializable;

/**
 * Transport type class responsible for returning a message of type String
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see Serializable
 * @see String
 */
public class ResponseMessageDTO implements Serializable {

	private static final long serialVersionUID = 8025904593912911583L;
	
	private Boolean success;
	private String message;
	private Integer id;
	
	/**
	 * Constructor with parameters
	 * @param result, result of an operation
	 * @param message, custom text message
	 * @see Boolean
	 * @see String
	 */
	public ResponseMessageDTO(final Boolean success, final String message) {
		this.setSucces(success);
		this.setMessage(message);
	}
	
	/**
	 * Constructor with parameters
	 * @param result, result of an operation
	 * @param message, custom text message
	 * @see Boolean
	 * @see String
	 */
	public ResponseMessageDTO(final Boolean success, final Integer id, final String message) {
		this.setId(id);
		this.setSucces(success);
		this.setMessage(message);
	}
	
	/**
	 * Method responsible for returning the value of the message attribute
	 * @return message, custom text message
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * Method responsible to assign the value of the message attribute
	 * @param message, custom text message
	 */
	public void setMessage(final String message) {
		this.message = message;
	}
	
	/**
	 * Method responsible for returning the assigned value as a result
	 * @return Boolean, stored result
	 */
	public Boolean getSuccess() {
		return success;
	}

	/**
	 * Method responsible for assigning a value to the result variable
	 * @param result, value to assign
	 */
	public void setSucces(final Boolean success) {
		this.success = success;
	}
	
	/**
	 * Method responsible for returning the value of the id attribute
	 * @return id, identifier to return
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Method responsible to assign the value of the id attribute
	 * @param id, identifier to return
	 */
	public void setId(Integer id) {
		this.id = id;
	}

}