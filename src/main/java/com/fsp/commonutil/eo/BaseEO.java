/*
 * Copyright Farmacias San Pablo
 * 30-09-2019
 */
package com.fsp.commonutil.eo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Abstract class is responsible for containing common fields for EO type classes
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see Serializable
 * @see Date
 * @see TemporalType
 * @see Boolean
 */
@MappedSuperclass
public abstract class BaseEO implements Serializable {

	private static final long serialVersionUID = -3986913068653602880L;
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_date", nullable = false, insertable = true, updatable = false)
	protected Date creationDate;
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "modification_date", nullable = true, insertable = false, updatable = true)
	protected Date modificationDate;
	@Column(name = "status", nullable = false)
	protected Boolean status;
	
	/**
	 * Method responsible for returning the creationDate
	 * @return Date
	 */
	public Date getCreationDate() {
		return creationDate;
	}
	
	/**
	 * Method responsible for assigning a value to the creationDate
	 * @param Date creationDate
	 */
	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}
	
	/**
	 * Method responsible for returning the id modificationDate
	 * @return Date
	 */
	public Date getModificationDate() {
		return modificationDate;
	}
	
	/**
	 * Method responsible for assigning a value to the modificationDate
	 * @param Date modificationDate
	 */
	public void setModificationDate(final Date modificationDate) {
		this.modificationDate = modificationDate;
	}
	
	/**
	 * Method responsible for returning the status
	 * @return Boolean
	 */
	public Boolean getStatus() {
		return status;
	}
	
	/**
	 * Method responsible for assigning a value to the status
	 * @param Boolean status
	 */
	public void setStatus(final Boolean status) {
		this.status = status;
	}

}