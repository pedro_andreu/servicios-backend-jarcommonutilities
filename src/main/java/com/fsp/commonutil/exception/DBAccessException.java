/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.commonutil.exception;

/**
 * Exception type class responsible for managing the errors that occur during connection to sap
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see Exception
 */
public class DBAccessException extends Exception {

	private static final long serialVersionUID = 5176660692971674994L;

	private final String uuid;
	
	/**
	 * Constructor with parameters
	 * @param Exception ex
	 * @param String UUID
	 * @see Exception
	 */
	public DBAccessException(Exception ex, String uuid) {
		super(ex);
		this.uuid = uuid;
	}
	
	/**
	 * Method responsible for returning a UUID
	 * @return String UUID
	 * @see String
	 */
	public String geUuid() {
		return this.uuid;
	}

}