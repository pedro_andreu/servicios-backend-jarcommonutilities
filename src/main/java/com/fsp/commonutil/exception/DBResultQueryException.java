/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.commonutil.exception;

/**
 * Exception type class responsible for managing errors that occur during transactions to the database
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see RuntimeException
 */
public class DBResultQueryException extends RuntimeException {

	private static final long serialVersionUID = 6993424866087327447L;
	
	private final String uuid;
	
	/**
	 * Constructor with parameters
	 * @param String message
	 * @param String UUID
	 * @see String
	 */
	public DBResultQueryException(String message, String uuid) {
		super(message);
		this.uuid = uuid;
	}
	
	/**
	 * Method responsible for returning a UUID
	 * @return String UUID
	 * @see String
	 */
	public String geUuid() {
		return this.uuid;
	}

}