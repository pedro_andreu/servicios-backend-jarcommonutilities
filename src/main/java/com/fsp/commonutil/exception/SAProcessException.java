/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.commonutil.exception;

/**
 * Filter type class responsible for filtering http requests and response
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see RuntimeException
 * @see String
 */
public class SAProcessException extends RuntimeException {

	private static final long serialVersionUID = -1296400463218462764L;
	
	private final String uuid;
	
	/**
	 * Constructor with parameters
	 * @param String message
	 * @param String UUID
	 * @see String
	 */
	public SAProcessException(final String message, final String uuid) {
		super(message);
		this.uuid = uuid;
	}
	
	/**
	 * Method responsible for returning a UUID
	 * @return String UUID
	 * @see String
	 */
	public String geUuid() {
		return this.uuid;
	}

}