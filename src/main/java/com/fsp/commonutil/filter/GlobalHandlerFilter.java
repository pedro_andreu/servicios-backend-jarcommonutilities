/*
 * Copyright Farmacias San Pablo
 * 03-10-2019
 */
package com.fsp.commonutil.filter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

/**
 * Filter type class responsible for filtering http requests and response
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see OncePerRequestFilter
 * @see Logger
 * @see LogManager
 * @see GlobalHandlerFilter
 * @see List
 * @see MediaType
 * @see Arrays
 */
public class GlobalHandlerFilter extends OncePerRequestFilter {
	
	private static final Logger log = Logger.getLogger(GlobalHandlerFilter.class);
	
	private static final List<MediaType> VISIBLE_TYPES = List.of(
			MediaType.valueOf("text/*"),
	        MediaType.APPLICATION_FORM_URLENCODED,
	        MediaType.APPLICATION_JSON,
	        MediaType.APPLICATION_XML,
	        MediaType.MULTIPART_FORM_DATA,
	        MediaType.valueOf("application/*+json"),
	        MediaType.valueOf("application/*+xml"),
	        MediaType.valueOf("multipart/*+form-data"));
	
	/**
	 * Provides HttpServletRequest and HttpServletResponse arguments instead of the default ServletRequest and ServletResponse ones
	 * @param HttpServletRequest request
	 * @param HttpServletResponse response
	 * @param FilterChain filterChain
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	protected void doFilterInternal(
			final HttpServletRequest request, 
			final HttpServletResponse response, 
			final FilterChain filterChain)
					throws ServletException, IOException {
		if (isAsyncDispatch(request)) {
            filterChain.doFilter(request, response);
        } else {
            doFilterWrapped(getWrapRequest(request), getWrapResponse(response), filterChain);
        }
	}
	
	/**
	 * Method responsible for filtering requests both request and response
	 * @param ContentCachingRequestWrapper request
	 * @param ContentCachingResponseWrapper response
	 * @param FilterChain filterChain
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doFilterWrapped(
			final ContentCachingRequestWrapper request, 
			final ContentCachingResponseWrapper response, 
			final FilterChain filterChain) 
					throws ServletException, IOException {
        try {
            beforeRequest(request, response);
            filterChain.doFilter(request, response);
        }
        finally {
            afterRequest(request, response);
            response.copyBodyToResponse();
        }
    }
	
	/**
	 * Method responsible for capturing request information
	 * @param ContentCachingRequestWrapper request
	 * @param ContentCachingResponseWrapper response
	 */
	protected void beforeRequest(
			final ContentCachingRequestWrapper request, 
			final ContentCachingResponseWrapper response) {
        readRequestHeader(request, request.getRemoteAddr());
    }

	/**
	 * Method responsible for capturing request information
	 * @param ContentCachingRequestWrapper request
	 * @param ContentCachingResponseWrapper response
	 */
    protected void afterRequest(
    		final ContentCachingRequestWrapper request, 
    		final ContentCachingResponseWrapper response) {
        readResponse(response, request.getRemoteAddr());
        readRequestBody(request, request.getRemoteAddr());
    }
	
    /**
	 * Method responsible for reading the content of a request header
	 * @param ContentCachingRequestWrapper request
	 * @param String prefix
	 */
    private static void readRequestHeader(
			final ContentCachingRequestWrapper request, 
			final String prefix) {
    	StringBuilder stbRequest = new StringBuilder();
		String queryString = request.getQueryString();
		stbRequest.append("Request: " + "\n");
        if (queryString == null) {
        	stbRequest.append("Remote Address: " + prefix + "\n");
        	stbRequest.append("HTTP Method: " + request.getMethod() + "\n");
        	stbRequest.append("Request URI: " + request.getRequestURI() + "\n");
        } else {
        	stbRequest.append("Remote Address: " + prefix + "\n");
        	stbRequest.append("HTTP Method: " + request.getMethod() + "\n");
        	stbRequest.append("Request URI: " + request.getRequestURI() + "\n");
        	stbRequest.append("QueryString: " + queryString + "\n");
        }
        stbRequest.append("Headers: " + "\n");
        Collections.list(request.getHeaderNames()).forEach(headerName -> 
                   Collections.list(request.getHeaders(headerName)).forEach(headerValue ->
                   stbRequest.append("- " + headerName + ": " + headerValue + "\n")));
        log.info(stbRequest.toString());
    }
	
	/**
	 * Method responsible for reading the content of a request body
	 * @param ContentCachingRequestWrapper request
	 * @param String prefix
	 */
	private static void readRequestBody(
			final ContentCachingRequestWrapper request, 
			final String prefix) {
		StringBuilder strRequestBody = new StringBuilder();
		byte[] content = request.getContentAsByteArray();
        if (content.length > 0) {
        	strRequestBody.append("Request body: " + "\n");
        	strRequestBody.append(readContent(content, request.getContentType(), request.getCharacterEncoding(), prefix));
        }
        log.info(strRequestBody.toString());
    }
	
	/**
	 * Method responsible for reading the content of a response
	 * @param ContentCachingResponseWrapper response
	 * @param String prefix
	 */
	private static void readResponse(
			final ContentCachingResponseWrapper response, 
			final String prefix) {
    	StringBuilder stbResponse = new StringBuilder();
        int status = response.getStatus();
        stbResponse.append("Response: " + "\n");
        stbResponse.append("Remote Address: " + prefix + "\n");
        stbResponse.append("HTTP Code: " + status + "\n");
        stbResponse.append("HTTP Status: " + HttpStatus.valueOf(status).getReasonPhrase() + "\n");
        stbResponse.append("Headers: " + "\n");
        response.getHeaderNames().forEach(headerName ->
        response.getHeaders(headerName).forEach(headerValue ->
        stbResponse.append("- " + headerName + ": " + headerValue + "\n")));
        byte[] content = response.getContentAsByteArray();
        if (content.length > 0) {
        	stbResponse.append("Response body: " + "\n");
        	stbResponse.append(readContent(content, response.getContentType(), response.getCharacterEncoding(), prefix));
        }
        log.info(stbResponse.toString());
    }
	
	/**
	 * Method responsible for reading the content of the petition
	 * @param byte[] content
	 * @param String contentType
	 * @param String contentEncoding
	 * @param String prefix
	 * @exception UnsupportedEncodingException ex
	 */
	private static String readContent(
			final byte[] content, 
			final String contentType, 
			final String contentEncoding, 
			final String prefix) {
		StringBuilder stbContent = new StringBuilder();
		MediaType mediaType = MediaType.valueOf(contentType);
		Boolean visible = VISIBLE_TYPES.stream().anyMatch(visibleType -> visibleType.includes(mediaType));
        if (visible) {
            try {
                String contentString = new String(content, contentEncoding);
                stbContent.append("- Body: " + "\n");
                Stream.of(contentString.split("\r\n|\r|\n")).forEach(line -> stbContent.append(line + "\n"));      
            } catch (UnsupportedEncodingException e) {
            	stbContent.append("- Exception: " + e.getMessage());
            }
        } 
        return stbContent.toString();
    }
	
	/**
	 * Method responsible for validating that the instance of an object respose is correct
	 * @param HttpServletResponse response
	 * @return response ContentCachingResponseWrapper
	 */
	private static ContentCachingRequestWrapper getWrapRequest(
			final HttpServletRequest request) {
        if (request instanceof ContentCachingRequestWrapper) {
            return (ContentCachingRequestWrapper) request;
        } else {
            return new ContentCachingRequestWrapper(request);
        }
    }
	
	/**
	 * Method responsible for validating that the instance of an object respose is correct
	 * @param HttpServletResponse response
	 * @return response ContentCachingResponseWrapper
	 */
	private static ContentCachingResponseWrapper getWrapResponse(
			final HttpServletResponse response) {
		if (response instanceof ContentCachingResponseWrapper) {
            return (ContentCachingResponseWrapper) response;
        } else {
            return new ContentCachingResponseWrapper(response);
        }
    }

}