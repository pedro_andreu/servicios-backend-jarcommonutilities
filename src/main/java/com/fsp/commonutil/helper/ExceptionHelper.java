/*
 * Copyright Farmacias San Pablo
 * 08-10-2019
 */
package com.fsp.commonutil.helper;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Helper type class responsible for converting between types for exception type classes
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 */
public final class ExceptionHelper {
	
	/**
	 * private default constructor
	 */
	private ExceptionHelper() {}
	
	/**
	 * Method responsible for converting a stack trace to string
	 * @param Throwable ex
	 * @return String printstackTrace
	 */
	public static String convertStackTraceToString(final Throwable ex) {
		StringWriter sw = new StringWriter();
		ex.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

}