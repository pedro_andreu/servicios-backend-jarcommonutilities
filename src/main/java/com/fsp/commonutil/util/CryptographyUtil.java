/*
 * Copyright Farmacias San Pablo
 * 08-10-2019
 */
package com.fsp.commonutil.util;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import com.fsp.commonutil.vo.CharacterEncodingVO;

/**
 * Utility class responsible for managing log access
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 */
public final class CryptographyUtil {
	
	/**
	 * Private constructor without parameters
	 */
	private CryptographyUtil() {}
	
	/**
	 * Method responsible for performing the encryption of a string
	 * @param String param
	 * @return String encode
	 * @throws UnsupportedEncodingException
	 * @see String
	 * @see Base64
	 * @see CharacterEncodingVO
	 */
	public static String encode(final String param) throws UnsupportedEncodingException {
		String encode = null;
		encode = Base64.getEncoder().encodeToString(param.getBytes(CharacterEncodingVO.UTF8));
		return encode;
	}
	
	/**
	 * Method in charge of decrypting a string
	 * @param String param
	 * @return String decode
	 * @throws UnsupportedEncodingException
	 * @see String
	 * @see Base64
	 * @see CharacterEncodingVO
	 */
	public static String decode(final String param) throws UnsupportedEncodingException {
		byte[] decode = null;
		decode = Base64.getDecoder().decode(param);
		return new String(decode, CharacterEncodingVO.UTF8);
	}

}