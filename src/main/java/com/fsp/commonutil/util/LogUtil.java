/*
 * Copyright Farmacias San Pablo
 * 08-10-2019
 */
package com.fsp.commonutil.util;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Utility class responsible for managing log access
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see Logger
 */
public final class LogUtil {
	
	static final Logger log = LogManager.getLogger(LogUtil.class.getName());
	
	/**
	 * Private constructor without parameters
	 */
	private LogUtil() {}
	
	/**
	 * Method in charge of storing information to the log
	 * @param Logger log
	 * @param String param
	 */
	public static void info(final Logger log, final String param) {
		log.info(param);
	}
	
	/**
	 * Method in charge of storing errors to the log
	 * @param Logger log
	 * @param String param
	 */
	public static void error(final Logger log, final String param) {
		log.error(param);
	}
	
	/**
	 * Method in charge of storing warnings to the log
	 * @param Logger log
	 * @param String param
	 */
	public static void warning(final Logger log, final String param) {
		log.warn(param);
	}

}