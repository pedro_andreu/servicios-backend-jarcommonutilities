/*
 * Copyright Farmacias San Pablo
 * 03-10-2019
 */
package com.fsp.commonutil.util;

import java.util.UUID;

/**
 * Utility class responsible for generating a UUID to trace requests
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 */
public final class UUIDUtil {
	
	/**
	 * Constructor without parameters
	 */
	private UUIDUtil() {}
	
	/**
	 * Method responsible for generating a random UUID to trace requests
	 * @return Strung uuid
	 */
	public static String getUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

}