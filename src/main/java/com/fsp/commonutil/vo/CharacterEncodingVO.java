/*
 * Copyright Farmacias San Pablo
 * 08-10-2019
 */
package com.fsp.commonutil.vo;

/**
 * VO type class responsible for storing values for character encoding
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see String
 */
public final class CharacterEncodingVO {
	
	/**
	 * Private constructor without parameters
	 */
	private CharacterEncodingVO() {}
	
	public static final String UTF8 = "utf-8";
	
}