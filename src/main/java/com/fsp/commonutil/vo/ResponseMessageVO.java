/*
 * Copyright Farmacias San Pablo
 * 02-10-2019
 */
package com.fsp.commonutil.vo;

/**
 * VO type class responsible for storing values for response messages in requests
 * @author Miguel Angel Gonzalez Gandara, angel.gonzalez@fsanpablo.com 
 * @version 1.0
 * @since 12
 * @see String
 */
public final class ResponseMessageVO {
	
	/**
	 * private default constructor
	 */
	private ResponseMessageVO() { }
	
	public static final String ERROR_MESSAGE = "Error";
    public static final String NO_RESULTS_FOUND = "No results found";
    public static final String THE_OPERATION_WAS_NOT_PROCESSED = "The operation was not processed";
    public static final String THE_OPERATION_HAS_BEEN_PROCESSED = "The operation has been processed";

}