/*
 * Copyright Farmacias San Pablo
 * 25-07-2019
 */
package com.fsp.commonutil;

/**
 * Class AppTest, Class responsible for being the starting point for the tests of the class.
 * @author Miguel Gandara {@link "mailto:angel.gonzalez@fsanpablo.com"} 
 * @version 1.0
 * @since 12
 */
public class AppTest {

}